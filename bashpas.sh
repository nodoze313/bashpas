function passgen() {
keylist="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#%$"
plength=12
temp=''
i=0
while [[ $i -lt $plength ]]
do
temp=$temp${keylist:$( shuf -i 0-${#keylist} -n 1 ):1}
i=$(( $i + 1 ))
done
echo $temp
}
function gpass() {
npass=10
p=0
while [[ $p -lt $npass ]]
do
passgen
p=$(( $p + 1 ))
done
}

gpass
